echo "Welcome to my linux install script"
echo "           Version 0.1"
#!/bin/bash
cd ~
echo "----------------------------------"
# Choice of package manager
echo "1. use apt"
echo "2. use pacman"
read answer

if [ $answer -eq 2 ];
then
	prompt="pacman";
	installprompt="sudo pacman -S";
	sudo pacman -Syu
else
	prompt="apt";
	installprompt="sudo apt install";
	sudo apt update;
	sudo apt upgrade
fi

clear
echo "----------------------------------"
echo "Packages should now be up to date"
echo "Installing essentials..."
packages="firefox wget zsh tmux python3 git"
$installprompt $packages

clear
echo "----------------------------------"
echo "Installing ohmyzsh..."
sh -c "$(wget https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)"

clear
echo "----------------------------------"
echo "Installing Fira Code font..."
mkdir source
cd source
git clone "git@github.com:tonsky/FiraCode.git"
cd FiraCode/distr/ttf/
xdg-open FiraCode-Regular.ttf
echo "Press enter when done installing font"
read trash

clear
echo "----------------------------------"
echo "Installing simpleterm..."
git clone "git://git.suckless.org/st"
cd st
wget "https://st.suckless.org/patches/dracula/st-dracula-20170803-7f99032.diff"
git checkout 7f99032
git apply st-dracula-20170803-7f99032.diff
cd ..

cd ..

