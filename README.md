# Linux install script

This will install the following tools and software, along with the required configuration for my own comfort:

## Essentials
- Firefox
- wget
- zsh
- ohmyzsh
- Fira Code font

## Terminal
- Simpleterm + vampire colorscheme
- Tmux
- powerline

## Programming
- Python3
- Pypy
- git
